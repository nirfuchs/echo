#!/bin/bash

echo 'Publish'
git_commit_hash=$(git rev-parse --short HEAD)
build_number=$BUILD_NUMBER
# echo git_commit_hash=$git_commit_hash
# echo build_number=$build_number


if [[ $BRANCH_NAME == "dev" ]]; then
  tag=dev-$git_commit_hash
fi

if [[ $BRANCH_NAME == "staging" ]]; then
  tag=staging-$git_commit_hash
fi

if [[ $BRANCH_NAME == "master" ]]; then
  tag=1.0.$build_number
fi

echo tag=$tag

# tag image
# docker tag nirfuchs73/echo eu.gcr.io/lustrous-setup-267010/echo:$tag
docker tag nirfuchs73/echo nirfuchs73/echo:$tag

# authenticate to Container Registry
# gcloud auth configure-docker

# push image
# docker push eu.gcr.io/lustrous-setup-267010/echo:$tag
docker push nirfuchs73/echo:$tag